import UserDomainInterface from "./user.domain.interface";

export default interface UserServiceInterface {
    login(email: string, password:string):Promise<any>,
    register(userDomain: UserDomainInterface): Promise<any>,
}