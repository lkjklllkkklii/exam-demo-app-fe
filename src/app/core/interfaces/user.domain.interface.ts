export default interface UserDomainInterface {
    id?:number, 
    email: string,  
    password:string, 
    firstName:string, 
    lastName: string
}