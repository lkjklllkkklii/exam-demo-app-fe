import { TestBed } from '@angular/core/testing';

import { HttpListenerInterceptor } from './http-listener.interceptor';

describe('HttpListenerInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpListenerInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: HttpListenerInterceptor = TestBed.inject(HttpListenerInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
