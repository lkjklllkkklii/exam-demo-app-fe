import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { MethodUtilsService } from '../utils/method-utils.service';
import {tap} from 'rxjs/operators';

@Injectable()
export class HttpListenerInterceptor implements HttpInterceptor {

  constructor(private _methodUtils: MethodUtilsService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(tap(()=> {}, (err:any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) return;
        this._methodUtils.navigate('/auth/sign-out');
      }
    }));
  }  
}


