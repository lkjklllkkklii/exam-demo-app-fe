import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const accessToken: string | null = localStorage.getItem('accessToken');
    if(!accessToken) return next.handle(request);
    const reqWithAuth = request.clone({
        setHeaders: { Authorization: `Bearer ${accessToken}` }
    })
    return next.handle(reqWithAuth);
  }
}
