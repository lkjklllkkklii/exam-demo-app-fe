import { Injectable } from '@angular/core';
import UserDomainInterface from '../interfaces/user.domain.interface';
import { ApiUtilsService } from '../utils/api-utils.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  route: string = "users";

  constructor(private _api: ApiUtilsService) { }

  login(email: string, password:string) {
    return this._api.post(this.route + '/login', {email, password});
  }

  register({firstName, lastName, email, password}: UserDomainInterface) {
    return this._api.post(this.route + '/register', {firstName, lastName, email, password});
  }

  findOneById(id:number) {
    return this._api.get(this.route +'/'+id);
  }

  findAll() {
    return this._api.get(this.route);
  }

}
