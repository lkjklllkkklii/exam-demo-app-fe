import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../http/user.service';

@Injectable({
  providedIn: 'root'
})
export class MethodUtilsService {

  constructor(
    private router: Router,
    // services
    private _userService: UserService) { }

  async registerAsync(firstName: string, lastName: string, email: string, password: string) {
    return await this._userService.register({firstName, lastName, email, password}).toPromise();    
  }

  async loginAsync(email: string, password: string) {
    return await this._userService.login(email, password).toPromise();
  }

  async findUserByIdAsync(id:number) {
    return await this._userService.findOneById(id).toPromise();
  }

  async findAllAsync() {
    return await this._userService.findAll().toPromise();
  }

  navigate = (route:string) => this.router.navigate([route]);

  setLocalSession = (key: string, value: string | any) => {
    if(typeof(value) !== 'string') value = JSON.stringify(value);
    localStorage.setItem(key, value);
  }
  getLocalSession = (key:string) => JSON.parse(localStorage.getItem(key) || '');
  clearAllLocalSession = () => localStorage.clear();
  clearLocalSession = (key:string) => localStorage.removeItem(key);

  delay = (ms:number) => new Promise(res => setTimeout(res, ms));

  addClass = (element:any, className:string) => element.addClass(className);
  removeClass = (element:any, className:string) => element.removeClass(className); 
}
