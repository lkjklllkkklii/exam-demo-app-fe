import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import ApiConfigService from '../config/api-config.service';

@Injectable({
  providedIn: 'root'
})
export class ApiUtilsService {
  private protocol = ApiConfigService.PROTOCOL;
  private hostname = ApiConfigService.HOSTNAME;
  private port = ApiConfigService.PORT;
  private prefix = ApiConfigService.PREFIX;
  private api = this.protocol + this.hostname + this.port + this.prefix;

  constructor(private http: HttpClient) { }

  get(route: string) {
    return this.http.get(this.api + route);
  }

  post(route: string, data: any) {
    return this.http.post(this.api + route, data);
  }

  put(route: string, data: any) {
    return this.http.put(this.api + route, data);
  }

  patch(route: string, data: any) {
    return this.http.put(this.api + route, data);
  }

  delete(route: string) {
    return this.http.delete(this.api + route);
  }
}
