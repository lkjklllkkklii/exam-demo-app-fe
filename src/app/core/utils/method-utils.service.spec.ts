import { TestBed } from '@angular/core/testing';

import { MethodUtilsService } from './method-utils.service';

describe('MethodUtilsService', () => {
  let service: MethodUtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MethodUtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
