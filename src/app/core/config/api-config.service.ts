import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export default class ApiConfigService {

  enableStaging: boolean = true;

  constructor() { }

  static get PROTOCOL() {
    return document.location.protocol + '//';
  }
  static get HOSTNAME() {
    return document.location.hostname;
  }
  static get PORT() {
    return ':8000/';
  }
  static get PREFIX() {
    return 'api/';
  }
}
