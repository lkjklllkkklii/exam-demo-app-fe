import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { MethodUtilsService } from '../utils/method-utils.service';

@Injectable({providedIn: 'root'})
export class AuthGuardService implements CanActivate {
  constructor(private _methodUtils:MethodUtilsService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const accessToken: string | null = localStorage.getItem('accessToken');
    console.log(accessToken);
    
    if(accessToken == null) return this._methodUtils.navigate('/auth/sign-in');
    return true;

  }
}