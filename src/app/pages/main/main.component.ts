import { Component, OnInit } from '@angular/core';
import { MethodUtilsService } from 'src/app/core/utils/method-utils.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  currentUser: any = this._methodUtils.getLocalSession('currentUser');
  users: any = {
    data: [],
    column: [
      {field: 'index', displayName: '#'},
      {field: 'email', displayName: 'Email'},
      {field: 'firstName', displayName: 'First Name'},
      {field: 'lastName', displayName: 'Last Name'},
      {field: 'createdAt', displayName: 'Date Created'},
    ]
  }
  public displayedColumns!: string[];

  constructor(private _methodUtils: MethodUtilsService) { }

  async ngOnInit(): Promise<any> {
    this.displayedColumns = this.users.column.map((a:any) => a.field);

    this.findAllUsers();
  }

  async findAllUsers() {
    const res: any = await this._methodUtils.findAllAsync();
    if(res.status.includes('success')) {
      this.users = {...this.users, data: res.data};
    } else console.log(res);    
  }

}
