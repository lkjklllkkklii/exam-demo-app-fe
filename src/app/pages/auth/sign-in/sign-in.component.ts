import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MethodUtilsService } from 'src/app/core/utils/method-utils.service';
declare var $:any;

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  form!: FormGroup;

  constructor(private _methodUtils:MethodUtilsService, private _formBuilder:FormBuilder) { }


  ngOnInit(): void {
    this.form = this._formBuilder.group({
      email: [],
      password: []
    });
  }

  async navigate(route:string) {
    const element = $('.card');
    this._methodUtils.removeClass(element, 'animate__animated animate__fadeInDown');
    this._methodUtils.addClass(element, 'animate__animated animate__fadeOutDown');
    await this._methodUtils.delay(500);
    this._methodUtils.navigate(route);
  }

  async loginAsync() {
    const {email, password} = this.form.value;
    const res: any = await this._methodUtils.loginAsync(email, password);
    console.log(res);
    if(res.status.includes("success")) {
      this._methodUtils.setLocalSession('accessToken', res.accessToken);
      this._methodUtils.setLocalSession('currentUser', res.user);
      this._methodUtils.navigate("/")
    } else this.form.reset();
  }

}
