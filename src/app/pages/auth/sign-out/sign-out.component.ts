import { Component, OnInit } from '@angular/core';
import { MethodUtilsService } from 'src/app/core/utils/method-utils.service';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.css']
})
export class SignOutComponent implements OnInit {

  constructor(private _methodUtils:MethodUtilsService) { }

  async ngOnInit() {
    await this._methodUtils.clearAllLocalSession();
    this._methodUtils.navigate('/auth/sign-in');
  }

}
