import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MethodUtilsService } from 'src/app/core/utils/method-utils.service';
declare var $:any;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

export class SignUpComponent implements OnInit {
  form!: FormGroup;

  constructor(private _methodUtils:MethodUtilsService, private _formBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      firstName: [],
      lastName: [],
      email: [],
      password: []
    });
  }
  
  async navigate(route:string) {
    const element = $('.card');
    this._methodUtils.removeClass(element, 'animate__animated animate__fadeInDown');
    this._methodUtils.addClass(element, 'animate__animated animate__fadeOutDown');
    await this._methodUtils.delay(500);
    this._methodUtils.navigate(route);
  }

  async registerAsync() {
    const {firstName, lastName, email, password} = this.form.value;
    const res: any = await this._methodUtils.registerAsync(firstName, lastName, email, password);
    console.log(res);
    if(res.status.includes("success")) this._methodUtils.navigate("/auth/sign-in");
    else this.form.reset();
  }

}
